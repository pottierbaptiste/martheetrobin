<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220414142803 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE articles CHANGE dateArticle postDate DATETIME NOT NULL, CHANGE titre title VARCHAR(255) NOT NULL, CHANGE typeArticle postType VARCHAR(10) NOT NULL, CHANGE qui who INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE articles CHANGE postDate dateArticle DATETIME NOT NULL, CHANGE title titre VARCHAR(255) NOT NULL, CHANGE postType typeArticle VARCHAR(10) NOT NULL, CHANGE who qui INT DEFAULT NULL');
    }
}
