<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220414143917 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_8F91ABF012836594');
        $this->addSql('ALTER TABLE comments CHANGE commentaire comment LONGTEXT DEFAULT NULL, CHANGE dateAvis commentDate DATETIME NOT NULL');
        $this->addSql('DROP INDEX idx_8f91abf012836594 ON comments');
        $this->addSql('CREATE INDEX IDX_5F9E962A12836594 ON comments (idArticle)');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_8F91ABF012836594 FOREIGN KEY (idArticle) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE likes DROP FOREIGN KEY FK_26A4AE877294869C');
        $this->addSql('ALTER TABLE likes DROP FOREIGN KEY FK_26A4AE87F675F31B');
        $this->addSql('DROP INDEX idx_26a4ae877294869c ON likes');
        $this->addSql('CREATE INDEX IDX_49CA4E7D7294869C ON likes (article_id)');
        $this->addSql('DROP INDEX idx_26a4ae87f675f31b ON likes');
        $this->addSql('CREATE INDEX IDX_49CA4E7DF675F31B ON likes (author_id)');
        $this->addSql('ALTER TABLE likes ADD CONSTRAINT FK_26A4AE877294869C FOREIGN KEY (article_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE likes ADD CONSTRAINT FK_26A4AE87F675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('DROP INDEX uniq_bfdd3168989d9b62 ON posts');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_885DBAFA989D9B62 ON posts (slug)');
        $this->addSql('DROP INDEX uniq_bfdd31688fa54d1b ON posts');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_885DBAFA8FA54D1B ON posts (visuel)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962A12836594');
        $this->addSql('ALTER TABLE comments CHANGE comment commentaire LONGTEXT DEFAULT NULL, CHANGE commentDate dateAvis DATETIME NOT NULL');
        $this->addSql('DROP INDEX idx_5f9e962a12836594 ON comments');
        $this->addSql('CREATE INDEX IDX_8F91ABF012836594 ON comments (idArticle)');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962A12836594 FOREIGN KEY (idArticle) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE likes DROP FOREIGN KEY FK_49CA4E7D7294869C');
        $this->addSql('ALTER TABLE likes DROP FOREIGN KEY FK_49CA4E7DF675F31B');
        $this->addSql('DROP INDEX idx_49ca4e7df675f31b ON likes');
        $this->addSql('CREATE INDEX IDX_26A4AE87F675F31B ON likes (author_id)');
        $this->addSql('DROP INDEX idx_49ca4e7d7294869c ON likes');
        $this->addSql('CREATE INDEX IDX_26A4AE877294869C ON likes (article_id)');
        $this->addSql('ALTER TABLE likes ADD CONSTRAINT FK_49CA4E7D7294869C FOREIGN KEY (article_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE likes ADD CONSTRAINT FK_49CA4E7DF675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('DROP INDEX uniq_885dbafa8fa54d1b ON posts');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BFDD31688FA54D1B ON posts (visuel)');
        $this->addSql('DROP INDEX uniq_885dbafa989d9b62 ON posts');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BFDD3168989D9B62 ON posts (slug)');
    }
}
