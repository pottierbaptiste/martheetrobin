<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220414143233 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('RENAME TABLE TLike TO likes');
        $this->addSql('RENAME TABLE articles TO posts');
        $this->addSql('RENAME TABLE avis TO comments');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('RENAME TABLE likes TO TLike');
        $this->addSql('RENAME TABLE posts TO articles');
        $this->addSql('RENAME TABLE comments TO avis');
    }
}
