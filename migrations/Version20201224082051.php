<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201224082051 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE articles ADD dateArticle DATETIME NOT NULL, ADD titre VARCHAR(255) DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL, ADD visuel VARCHAR(255) NOT NULL, ADD typeArticle VARCHAR(10) NOT NULL, ADD qui INT DEFAULT NULL, ADD createdAt DATETIME DEFAULT NULL, ADD updatedAt DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE articles DROP dateArticle, DROP titre, DROP description, DROP visuel, DROP typeArticle, DROP qui, DROP createdAt, DROP updatedAt');
    }
}
