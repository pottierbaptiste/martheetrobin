.DEFAULT_GOAL=test

# Aliases
DOCKER_COMPOSE = docker compose
DOCKER_COMPOSE_EXEC = $(DOCKER_COMPOSE) exec it

build:
	USER_ID=$(shell id -u) GROUP_ID=$(shell id -g) $(DOCKER_COMPOSE) up --build -d
up:
	USER_ID=$(shell id -u) GROUP_ID=$(shell id -g) $(DOCKER_COMPOSE) up -d
#stop:
#	USER_ID=$(shell id -u) GROUP_ID=$(shell id -g) $(DOCKER_COMPOSE) down --remove-orphans
docker-db:
	$(DOCKER_COMPOSE_EXEC) database bash


install: composer.json
	composer install
reset-db:
	./bin/console doctrine:database:drop --force --if-exists
	./bin/console doctrine:database:create
	./bin/console doctrine:migrations:migrate --no-interaction
fixtures: reset-db
	./bin/console doctrine:fixtures:load --no-interaction
test:
	./bin/console doctrine:database:drop --if-exists --force --env=test
	./bin/console doctrine:database:create --env=test
	./bin/console doctrine:migrations:migrate --no-interaction --env=test
	./vendor/bin/phpunit
format:
	./vendor/bin/pint -v
lint:
	./bin/console lint:twig templates
	./bin/console lint:container
	./vendor/bin/php-cs-fixer fix -v
	./vendor/bin/phpstan -l9 analyse src
deploy-server:
	git pull
	php composer.phar install --no-dev -o
	php ./bin/console doctrine:migrations:migrate --no-interaction
all: install fixtures tests format lint

init:
	make docker-install
	make devbox-shell
	make install
	php -S 127.0.0.1:8000 -t ./public
server:
	devbox run server
start:
	docker compose up -d
	symfony server:start -d --no-tls
stop:
	symfony server:stop
	docker compose down

.PHONY: install reset-db fixtures tests format lint deploy-server init debox-shell docker-install server build up stop