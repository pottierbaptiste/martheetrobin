À faire sur l'environnement de dev :

```
docker compose up -d
./php composer install
sudo chmod 777 -R .
//dans le .env : 
DATABASE_URL=mysql://root:root@db_docker_symfony:3306/martheetrobin
./php bin/console doctrine:database:create
./php bin/console doctrine:migrations:migrate
./php bin/console doctrine:fixtures:load -n
./php ./bin/phpunit
./php ./vendor/bin/phpstan -l2 analyse src tests
```

acces web : pottierbaptiste@gmail.com / pwdDev

les commandes à lancer depuis le contener php (nodes, php, composer, symfony cli, ... )  peuvent se lancer via le fichier de commande bash "php" , 
```
exemple : ./php composer update
```
