<?php

namespace App\Twig\Components;

use App\Entity\Article;
use Symfony\Component\Form\FormView;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
final class ArticleComponent
{
    public Article $article;
    public FormView $formCommentaire;
}
