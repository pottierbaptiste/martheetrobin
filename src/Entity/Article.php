<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
#[ORM\Table(name: 'posts')]
#[UniqueEntity('slug')]
#[UniqueEntity('visuel')]
#[Vich\Uploadable]
class Article
{
    final public const KIDS = [
        '0' => [
            'prenom' => 'Marthe',
            'className' => 'marthe',
            'id' => 0],
        '1' => [
            'prenom' => 'Robin',
            'className' => 'robin',
            'id' => 1],
        '2' => [
            'prenom' => 'Les deux',
            'className' => 'lesdeux',
            'id' => 2],
    ];

    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\Id, ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(name: 'postDate', type: Types::DATETIME_MUTABLE)]
    #[Assert\NotBlank]
    private \DateTimeInterface $dateArticle;

    #[ORM\Column(name: 'title', type: Types::STRING, length: 255)]
    #[Assert\NotBlank]
    private string $titre;

    #[Gedmo\Slug(fields: ['titre'])]
    #[ORM\Column(name: 'slug', type: Types::STRING, length: 100, unique: true)]
    private string $slug;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::STRING, length: 100, unique: true)]
    private ?string $visuel;

    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'visuel')]
    #[Assert\File(mimeTypes: ['image/jpeg', 'image/gif', 'image/png'])]
    private ?File $imageFile = null;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $video;

    #[Vich\UploadableField(mapping: 'article_video', fileNameProperty: 'video')]
    #[Assert\File(mimeTypes: ['video/3gpp2', 'video/3gpp', 'video/ogg', 'video/mp4', 'video/mpeg', 'video/webm', 'video/x-msvideo'])]
    private ?File $videoFile = null;

    #[ORM\Column(name: 'postType', type: Types::STRING, length: 10)]
    #[Assert\Choice(['image', 'video'])]
    private string $typeArticle;

    #[ORM\Column(name: 'who', type: Types::INTEGER, nullable: true)]
    #[Assert\Choice([0, 1, 2])]
    private int $qui;

    public string $chemin;

    /**
     * @var Collection<int, Commentaire>
     */
    #[ORM\OneToMany(mappedBy: 'article', targetEntity: 'App\Entity\Commentaire', cascade: ['persist', 'remove'], orphanRemoval: true)]
    private Collection $avis;

    /**
     * @var Collection<int, Like>
     */
    #[ORM\OneToMany(mappedBy: 'article', targetEntity: 'App\Entity\Like', cascade: ['persist', 'remove'], orphanRemoval: true)]
    private Collection $likes;

    #[ORM\Column(name: 'createdAt', type: Types::DATETIME_MUTABLE, nullable: true)]
    private \DateTimeInterface $createdAt;

    #[ORM\Column(name: 'updatedAt', type: Types::DATETIME_MUTABLE, nullable: true)]
    private \DateTimeInterface $updatedAt;

    public function __construct(
        string $titre,
        \DateTimeImmutable $dateArticle,
        string $typeArticle,
    ) {
        $this->titre = $titre;
        $this->dateArticle = $dateArticle;
        $this->typeArticle = $typeArticle;
        $this->avis = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->likes = new ArrayCollection();
        $this->visuel = null;
        $this->video = null;
        $this->imageFile = null;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     */
    public function setImageFile(File|UploadedFile|null $image = null): self
    {
        $this->imageFile = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime();
        }

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDateArticle(): \DateTimeInterface
    {
        return $this->dateArticle;
    }

    public function setDateArticle(\DateTimeInterface $dateArticle): self
    {
        $this->dateArticle = $dateArticle;

        return $this;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVisuel(): ?string
    {
        return $this->visuel;
    }

    public function setVisuel(?string $visuel): self
    {
        $this->visuel = $visuel;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getTypeArticle(): string
    {
        return $this->typeArticle;
    }

    public function setTypeArticle(string $typeArticle): self
    {
        $this->typeArticle = $typeArticle;

        return $this;
    }

    public function getQui(): int
    {
        return $this->qui;
    }

    public function setQui(int $qui): self
    {
        $this->qui = $qui;

        return $this;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, Commentaire>
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    /**
     * @return Collection<int, Like>
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setArticle($this);
        }

        return $this;
    }

    public function removeLike(Like $like): self
    {
        if ($this->likes->contains($like)) {
            $this->likes->removeElement($like);
            // set the owning side to null (unless already changed)
            if ($like->getArticle() === $this) {
                $like->setArticle(null);
            }
        }

        return $this;
    }

    public function getChemin(): string
    {
        // le 19/12/2021 suite à phpstan : Call to function is_null() with int will always evaluate to false.
        // if (is_null($this->getQui())){
        //    return '#';
        // }
        $kid = self::KIDS[$this->getQui()];

        return '/'.$kid['className'].'/'.($this->getSlug() ?? $this->getId());
    }

    public function getEnfant(): string
    {
        $kid = self::KIDS[$this->getQui()];

        return $kid['className'];
    }

    public function getVideoFile(): ?File
    {
        return $this->videoFile;
    }

    public function setVideoFile(File|UploadedFile|null $videoFile = null): self
    {
        $this->videoFile = $videoFile;

        if (null !== $videoFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function toggleLike(UserInterface $user): bool
    {
        $like = $this->getLikeFromUser($user);
        if ($like) {
            $this->removeLike($like);

            return false;
        } else {
            $like = new Like($this, $user);
            $this->addLike($like);

            return true;
        }
    }

    private function getLikeFromUser(UserInterface $user): ?Like
    {
        foreach ($this->getLikes() as $like) {
            if ($like->getAuthor() == $user) {
                return $like;
            }
        }

        return null;
    }
}
