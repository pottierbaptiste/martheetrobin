<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CommentaireRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommentaireRepository::class)]
#[ORM\Table(name: 'comments')]
class Commentaire
{
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\Id, ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(name: 'comment', type: Types::TEXT, nullable: true)]
    private string $commentaire;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $signature;

    #[ORM\Column(name: 'commentDate', type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $dateAvis;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\Article', inversedBy: 'avis')]
    #[ORM\JoinColumn(name: 'idArticle', referencedColumnName: 'id')]
    private Article $article;

    public function __construct(string $commentaire, Article $article, string $signature, ?\DateTime $dateAvis = null)
    {
        $this->setCommentaire($commentaire);
        $this->setArticle($article);
        $this->setSignature($signature);
        $this->setDateAvis($dateAvis ?? new \DateTime());
    }

    public static function create(string $commentaire, Article $article, string $signature): Commentaire
    {
        return new self($commentaire, $article, $signature);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    private function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getSignature(): ?string
    {
        return $this->signature;
    }

    private function setSignature(string $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    public function getDateAvis(): ?\DateTimeInterface
    {
        return $this->dateAvis;
    }

    private function setDateAvis(\DateTimeInterface $dateAvis): self
    {
        $this->dateAvis = $dateAvis;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(Article $article): self
    {
        $this->article = $article;

        return $this;
    }
}
