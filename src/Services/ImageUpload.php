<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Article;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUpload
{
    public function __construct(private readonly string $targetDir)
    {
    }

    public function upload(UploadedFile $file, Article $article): string
    {
        $fileName = $article->getId().'-'.\md5(uniqid()).'.'.$file->guessExtension();
        $file->move($this->getTargetDir(), $fileName);

        return $fileName;
    }

    public function getTargetDir(): string
    {
        return $this->targetDir;
    }
}
