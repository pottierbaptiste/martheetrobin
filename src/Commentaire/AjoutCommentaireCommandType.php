<?php

declare(strict_types=1);

namespace App\Commentaire;

use App\Entity\Article;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AjoutCommentaireCommandType extends AbstractType implements DataMapperInterface
{
    public function __construct(private readonly ArticleRepository $articleRepository, private readonly UserRepository $userRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Article $article */
        $article = $options['article'];

        /** @var User $signataire */
        $signataire = $options['signataire'];

        $builder
            ->add('commentaire', TextareaType::class)
            ->add('article', HiddenType::class, [
                'data' => $article->getId(),
            ])
            ->add('signataire', HiddenType::class, [
                'data' => $signataire->getId(),
            ])

            ->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'article' => null,
            'signataire' => null,
        ]);

        $resolver->setAllowedTypes('article', Article::class); /* utilité ? */
        $resolver->setAllowedTypes('signataire', User::class);
    }

    public function mapDataToForms($viewData, $forms): void
    {
        if (null === $viewData) {
            return;
        }

        if (!$viewData instanceof AjoutCommentaireCommand) {
            throw new UnexpectedTypeException($viewData, AjoutCommentaireCommand::class);
        }

        $forms = iterator_to_array($forms);
        $forms['commentaire']->setData($viewData->commentaire());
        $forms['article']->setData($viewData->article());
        $forms['signataire']->setData($viewData->signataire());
    }

    public function mapFormsToData($forms, &$viewData): void
    {
        /** @var FormInterface[] $forms */
        $forms = iterator_to_array($forms);

        /** @var string $commentaire */
        $commentaire = $forms['commentaire']->getData();
        /** @var Article $article */
        $article = $this->articleRepository->find($forms['article']->getData());
        /** @var User $signataire */
        $signataire = $this->userRepository->find($forms['signataire']->getData());

        $viewData = new AjoutCommentaireCommand($commentaire, $article, $signataire);
    }
}
