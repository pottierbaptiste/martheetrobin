<?php

declare(strict_types=1);

namespace App\Commentaire;

use App\Entity\Article;
use App\Entity\User;

class AjoutCommentaireCommand
{
    public function __construct(private readonly string $commentaire, private readonly Article $article, private readonly User $signataire)
    {
    }

    public function commentaire(): string
    {
        return $this->commentaire;
    }

    public function article(): Article
    {
        return $this->article;
    }

    public function signataire(): User
    {
        return $this->signataire;
    }
}
