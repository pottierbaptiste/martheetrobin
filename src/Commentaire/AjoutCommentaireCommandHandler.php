<?php

declare(strict_types=1);

namespace App\Commentaire;

use App\Entity\Commentaire;
use App\Repository\CommentaireRepository;
use Doctrine\ORM\EntityManagerInterface;

class AjoutCommentaireCommandHandler
{
    public function __construct(private readonly CommentaireRepository $commentaireRepository, private readonly EntityManagerInterface $em)
    {
    }

    public function execute(AjoutCommentaireCommand $ajoutCommentaireCommand): void
    {
        $commentaire = Commentaire::create($ajoutCommentaireCommand->commentaire(), $ajoutCommentaireCommand->article(), (string) $ajoutCommentaireCommand->signataire()->getUsername());

        if ($this->ceCommentaireExisteDeja($commentaire)) {
            $this->em->persist($commentaire);
            $this->em->flush();
        }
    }

    private function ceCommentaireExisteDeja(Commentaire $commentaire): bool
    {
        $avisClone = $this->commentaireRepository->findOneBy(
            [
                'commentaire' => $commentaire->getCommentaire(),
                'signature' => $commentaire->getSignature(),
                'article' => $commentaire->getArticle(),
            ]);

        return is_null($avisClone);
    }
}
