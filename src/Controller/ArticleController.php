<?php

declare(strict_types=1);

namespace App\Controller;

use App\Commentaire\AjoutCommentaireCommand;
use App\Commentaire\AjoutCommentaireCommandHandler;
use App\Commentaire\AjoutCommentaireCommandType;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Cache\ItemInterface;

class ArticleController extends AbstractController
{
    public function __construct(private readonly AjoutCommentaireCommandHandler $ajoutCommentaireCommandHandler, private readonly ArticleRepository $articleRepository)
    {
    }

    #[Route('/like/{slug}', name: 'mr_like_vote')]
    #[IsGranted('ROLE_USER')]
    public function likeVote(Article $article, EntityManagerInterface $manager): RedirectResponse
    {
        /** @var UserInterface */
        $currentUser = $this->getUser();
        $article->toggleLike($currentUser);

        $manager->persist($article);
        $manager->flush();

        return $this->redirectToRoute('mr_article_single', ['enfant' => $article->getEnfant(), 'slug' => $article->getSlug()]);
    }

    #[Route(
        '/{enfant}/{slug}',
        name: 'mr_article_single',
        requirements: [
            'enfant' => '^[mrl]\w+',
            'slug' => '[a-zA-Z0-9\-_\/]+',
        ]
    )]
    #[IsGranted('ROLE_USER')]
    #[Cache(maxage: 3600, public: true, mustRevalidate: true)]
    public function single(string $enfant, string $slug, Request $request, SessionInterface $session): Response
    {
        $cache = new FilesystemAdapter();
        /** @var Article $article */
        $article = $cache->get('sluged_article'.$slug, function (ItemInterface $item) use ($slug) {
            $item->expiresAfter(30);

            return $this->articleRepository->getFromSlug($slug);
        });

        $lienRetourListe = $session->get('lienRetourListe') ?: $this->generateUrl('mr_index');

        $formCommentaire = $this->createForm(AjoutCommentaireCommandType::class, null, [
            'article' => $article,
            'signataire' => $this->getUser(),
        ]);
        $formCommentaire->handleRequest($request);

        /** @var AjoutCommentaireCommand $ajoutCommentaireCommand */
        $ajoutCommentaireCommand = $formCommentaire->getData();

        if ($formCommentaire->isSubmitted() && $formCommentaire->isValid()) {
            $this->ajoutCommentaireCommandHandler->execute($ajoutCommentaireCommand);
            $cache->delete('sluged_article'.$slug);

            return $this->redirectToRoute('mr_article_single', ['enfant' => $article->getEnfant(), 'slug' => $article->getSlug()]);
        }

        return $this->render('single.html.twig', [
            'article' => $article,
            'lienRetourListe' => $lienRetourListe,
            'formCommentaire' => $formCommentaire,
        ]);
    }
}
