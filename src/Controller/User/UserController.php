<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class UserController extends AbstractController
{
    #[Route('/account/resetpwd', name : 'mr_security_resetpwd')]
    #[IsGranted('ROLE_USER')]
    public function resetpwd(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createFormBuilder($user)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class, ['label' => 'enregistrer ce mot de passe'])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->hashPassword($user, (string) $user->getPassword());

            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('mr_index');
        }

        return $this->render('security/genericForm.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/account/settingNewsletter', name : 'mr_security_settingNewsletter')]
    #[IsGranted('ROLE_USER')]
    public function settingNewsletter(Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createFormBuilder([
            'user' => $user,
        ])
            ->add('wantNewsByEmail', CheckboxType::class, [
                'label' => ' Je desire recevoir le recap hebdo',
                'data' => $user->getWantNewsByEmail(),
                'required' => false,
            ])
            ->add('save', SubmitType::class, ['label' => 'enregistrer mon choix'])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var bool $wantNewsletter */
            $wantNewsletter = $form->get('wantNewsByEmail')->getData();
            $message = $wantNewsletter ? 'Vous êtes désormais inscrit à la newsletter' : 'Désinscription entregistrée';
            $user->setWantNewsByEmail($wantNewsletter);
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', $message);
        }

        return $this->render('security/genericForm.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
