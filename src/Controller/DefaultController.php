<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Cache\ItemInterface;

class DefaultController extends AbstractController
{
    final public const nbMaxSearchResult = 30;
    final public const nbArticlePerPage = 5;

    #[Route('/{page}', name: 'mr_index', requirements: ['page' => '\d+'], defaults: ['page' => '1'])]
    #[IsGranted('ROLE_USER')]
    #[Cache(maxage: 3600, public: true, mustRevalidate: true)]
    public function index(Request $request, SessionInterface $session, ArticleRepository $repoArticle, int $page = 1): Response
    {
        // NB : ce principe de mise en cache ne semble pas efficace (la requete reste jouée en env de dev du moins)
        // la commande suivante est tout aussi bien :
        // $articles = $repoArticle->findAllPagineEtTrie($page, self::nbArticlePerPage);
        $cache = new FilesystemAdapter();
        $articles = $cache->get('paginated_article'.$page, function (ItemInterface $item) use ($repoArticle, $page) {
            $item->expiresAfter(20);

            return $repoArticle->findAllPagineEtTrie($page, self::nbArticlePerPage);
        });

        // $articles = $repoArticle->findAllPagineEtTrie($page, self::nbArticlePerPage);
        $pagination = [
            'page' => $page,
            'nbPages' => ceil(count($articles) / self::nbArticlePerPage),
            'nomRoute' => 'mr_index',
            'paramsRoute' => [],
        ];

        $session->set('lienRetourListe', $request->getUri());

        return $this->render('index.html.twig', [
            'articles' => $articles,
            'pagination' => $pagination,
        ]);
    }

    #[Route('/recherche/{page}', name: 'mr_search')]
    #[IsGranted('ROLE_USER')]
    public function search(ArticleRepository $articleRepository, Request $request, string $elementRecherche = '', int $page = 1): Response
    {
        $elementRecherche = $request->query->get('keyword');
        $elementRecherche ??= 'motquinexisterapas';
        $articles = $articleRepository->searchTerms((string) $elementRecherche, $page, self::nbMaxSearchResult);
        $pagination = [
            'page' => $page,
            'nbPages' => ceil(count($articles) / self::nbMaxSearchResult),
            'nomRoute' => 'mr_search',
            'paramsRoute' => [],
        ];

        $messageInformation = '';
        if (count($articles) > self::nbMaxSearchResult) {
            $messageInformation = "le nombre d'articles correspondant à votre recherche dépasse ".self::nbMaxSearchResult.'. ';
            $messageInformation .= "Nous n'affichons ici que les ".self::nbMaxSearchResult.' premiers';
        }

        return $this->render('recherche.html.twig', [
            'articles' => $articles,
            'pagination' => $pagination,
            'nbArticles' => count($articles),
            'messageInfo' => $messageInformation,
        ]);
    }
}
