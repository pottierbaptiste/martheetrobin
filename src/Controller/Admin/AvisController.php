<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\Commentaire;
use App\Repository\CommentaireRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AvisController extends AbstractController
{
    #[Route('/admin/avis', name : 'mr_admin_avis')]
    public function listeAvis(CommentaireRepository $repoAvis): Response
    {
        $allAvis = $repoAvis->findBy([], ['id' => 'desc']);
        $Comments = [];

        /** @var Commentaire $avis */
        foreach ($allAvis as $avis) {
            $Comments[] = $avis->getSignature().' : '.$avis->getCommentaire();
            /** @var Article $article */
            $article = $avis->getArticle();
            $Comments[] = $article->getChemin()."<a href='".$article->getChemin()."' > voir</a>";
        }

        return $this->render('admin/commentaires.html.twig', [
            'allAvis' => $allAvis,
            'commentaires' => $Comments,
        ]);
    }

    #[Route('/admin/avis/{avis}/delete', name : 'mr_admin_deleteAvis')]
    public function deleteAvis(Commentaire $avis, EntityManagerInterface $manager): Response
    {
        $manager->remove($avis);
        $manager->flush();

        return $this->redirectToRoute('mr_admin_avis');
    }
}
