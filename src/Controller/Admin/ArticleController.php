<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Article\AjoutArticleCommand;
use App\Article\AjoutArticleCommandHandler;
use App\Article\AjoutArticleCommandType;
use App\Article\ModifieArticleCommand;
use App\Article\ModifieArticleCommandHandler;
use App\Article\ModifieArticleCommandType;
use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ArticleController extends AbstractController
{
    public function __construct(
        private readonly AjoutArticleCommandHandler $ajoutArticleCommandHandler,
        private readonly ModifieArticleCommandHandler $modifieArticleCommandHandler,
    ) {
    }

    #[Route('/admin/articles/create', name : 'mr_admin_addArticle')]
    public function AddArticle(Request $request): Response
    {
        $formArticle = $this->createForm(AjoutArticleCommandType::class);
        $formArticle->handleRequest($request);

        /** @var AjoutArticleCommand $ajoutArticleCommand */
        $ajoutArticleCommand = $formArticle->getData();

        if ($formArticle->isSubmitted() && $formArticle->isValid()) {
            $articleReturned = $this->ajoutArticleCommandHandler->execute($ajoutArticleCommand);

            return $this->redirectToRoute('mr_article_single', ['enfant' => $articleReturned->getEnfant(), 'slug' => $articleReturned->getSlug()]);
        }

        return $this->render('admin/addArticle.html.twig', [
            'form' => $formArticle,
            'labelSubmit' => 'ajouter cet article',
        ]);
    }

    #[Route('/admin/articles/{article}/update', name : 'mr_admin_updateArticle')]
    public function updateArticle(Article $article, Request $request, UploaderHelper $helper): Response
    {
        $imageFile = $videoFile = null;
        /** @var string $imageFilePath */
        $imageFilePath = $helper->asset($article, 'imageFile');
        if (is_file($imageFilePath)) {
            $imageFile = new File($imageFilePath, checkPath: true);
        }

        /** @var string|null $videoFilePath */
        $videoFilePath = $helper->asset($article, 'videoFile');
        if ($videoFilePath && is_file($videoFilePath)) {
            $videoFile = new File($videoFilePath, checkPath: true);
        }

        $modifieArticleCommand = ModifieArticleCommand::fabrique(
            id: $article->getId(),
            titre: $article->getTitre(),
            description: $article->getDescription(),
            date: $article->getDateArticle(),
            imageFile: $imageFile,
            videoFile: $videoFile,
            type: $article->getTypeArticle(),
            qui: $article->getQui(),
            visuel: $imageFile?->getFilename(),
            video: $videoFile?->getFilename()
        );

        $form = $this->createForm(ModifieArticleCommandType::class, $modifieArticleCommand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ModifieArticleCommand $modifieArticleCommand */
            $modifieArticleCommand = $form->getData();
            $article = $this->modifieArticleCommandHandler->execute($modifieArticleCommand);

            $cache = new FilesystemAdapter();
            $cache->delete('sluged_article'.$article->getSlug());

            return $this->redirectToRoute('mr_article_single', ['enfant' => $article->getEnfant(), 'slug' => $article->getSlug()]);
        }

        return $this->render('admin/updateArticle.html.twig', [
            'article' => $article,
            'form' => $form,
            'labelSubmit' => "modifier l'article",
        ]);
    }

    #[Route('/admin/articles/{article}/delete', name : 'mr_admin_deleteArticle')]
    public function deleteArticle(Article $article, EntityManagerInterface $manager): Response
    {
        $manager->remove($article);
        $manager->flush();

        return $this->redirectToRoute('mr_index');
    }
}
