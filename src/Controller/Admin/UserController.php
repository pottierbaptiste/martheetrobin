<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class UserController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly CacheInterface $cache)
    {
    }

    #[Route('/admin/users/create', name : 'mr_security_registration')]
    public function inscription(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->hashPassword($user, (string) $user->getPassword());

            $user->setPassword($hash);
            $user->setRoles(['ROLE_USER']);

            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->cache->delete('all_users');

            return $this->redirectToRoute('mr_admin_user_list');
        }

        return $this->render('security/registration.html.twig', [
            'formUser' => $form,
        ]);
    }

    #[Route('/admin/users', name : 'mr_admin_user_list')]
    public function listeUser(UserRepository $repoUser): Response
    {
        // $allUsers = $repoUser->findAll();
        $allUsers = $this->cache->get('all_users', function (ItemInterface $item) use ($repoUser) {
            $item->expiresAfter(20);

            return $repoUser->findAll();
        });

        return $this->render('admin/users.html.twig', [
            'allUsers' => $allUsers,
        ]);
    }

    #[Route('/admin/users/{user}/overwritepassworduser', name : 'mr_security_overwrite_password_user')]
    public function overwritePasswordUser(User $user, Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder): Response
    {
        $form = $this->createFormBuilder($user)
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class, ['label' => 'enregistrer ce mot de passe'])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->hashPassword($user, (string) $user->getPassword());

            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
            $this->cache->delete('all_users');

            return $this->redirectToRoute('mr_index');
        }

        return $this->render('security/genericForm.html.twig', [
            'form' => $form,
        ]);
    }
}
