<?php

namespace App\Article;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dateArticle', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('titre')
            ->add('description')
            ->add('imageFile', VichImageType::class, [
                'label' => 'visuel',
                'required' => false,
                'imagine_pattern' => 'admin_thumbnail',
                'allow_delete' => false,
                'download_uri' => false,
            ])
            ->add('videoFile', VichFileType::class, [
                'label' => 'video',
                'required' => false,
                'allow_delete' => true,
                'download_uri' => false,
                'delete_label' => 'supprimer la video',
                'download_label' => 'télécharger',
                'asset_helper' => true,
            ])
            ->add('typeArticle', ChoiceType::class, [
                'label' => 'Type d\'article',
                'choices' => [
                    'image' => 'image',
                    'video' => 'video',
                ],
            ])
            ->add('qui', ChoiceType::class, [
                'label' => 'De qui qu\'on cause ?',
                'choices' => $this->listeSelect(),
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            '$options' => null,
        ]);
    }

    /**
     * @return array<string, int>
     */
    private function listeSelect(): array
    {
        $tabKids = [];
        foreach (Article::KIDS as $kid) {
            $tabKids[$kid['prenom']] = $kid['id'];
        }

        return $tabKids;
    }
}
