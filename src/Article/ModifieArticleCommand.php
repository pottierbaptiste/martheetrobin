<?php

namespace App\Article;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
class ModifieArticleCommand
{
    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'visuel')]
    public ?File $imageFile;

    #[Vich\UploadableField(mapping: 'article_video', fileNameProperty: 'video')]
    public ?File $videoFile = null;

    private function __construct(
        public int $id,
        public string $titre,
        public ?string $description,
        public \DateTimeInterface $date,
        public string $type,
        public int $qui,
        public ?string $visuel,
        public ?string $video,
    ) {
    }

    public static function fabrique(
        int $id,
        string $titre,
        ?string $description,
        \DateTimeInterface $date,
        ?File $imageFile,
        ?File $videoFile,
        string $type,
        int $qui,
        ?string $visuel,
        ?string $video,
    ): self {
        $modifieArticleCommand = new self(
            id: $id,
            titre: $titre,
            description: $description,
            date: $date,
            type: $type,
            qui: $qui,
            visuel: $visuel,
            video: $video,
        );
        $modifieArticleCommand->imageFile = $imageFile;
        $modifieArticleCommand->videoFile = $videoFile;

        return $modifieArticleCommand;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getVideoFile(): ?File
    {
        return $this->videoFile;
    }

    public function getQui(): ?int
    {
        return $this->qui;
    }

    public function getType(): ?string
    {
        return $this->type;
    }
}
