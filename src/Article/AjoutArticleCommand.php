<?php

namespace App\Article;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
class AjoutArticleCommand
{
    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'visuel')]
    #[Assert\File(mimeTypes: ['image/jpeg', 'image/gif', 'image/png'])]
    public File $imageFile;

    #[Vich\UploadableField(mapping: 'article_video', fileNameProperty: 'video')]
    public ?File $videoFile = null;

    private function __construct(
        public string $titre,
        public string $description,
        public \DateTimeImmutable $date,
        public string $type,
        public int $qui,
    ) {
    }

    public static function fabrique(
        string $titre,
        string $description,
        \DateTimeImmutable $date,
        File $imageFile,
        ?File $videoFile,
        string $type,
        int $qui,
    ): self {
        $ajoutArticleCommand = new self(
            titre: $titre,
            description: $description,
            date: $date,
            type: $type,
            qui: $qui,
        );
        $ajoutArticleCommand->imageFile = $imageFile;
        $ajoutArticleCommand->videoFile = $videoFile;

        return $ajoutArticleCommand;
    }
}
