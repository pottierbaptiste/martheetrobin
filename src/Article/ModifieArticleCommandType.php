<?php

namespace App\Article;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ModifieArticleCommandType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('dateArticle', DateTimeType::class)
            ->add('titre')
            ->add('description', TextareaType::class)
            ->add('imageFile', VichImageType::class, [
                'label' => 'image',
                'required' => false,
                'imagine_pattern' => 'admin_thumbnail',
                'allow_delete' => false,
                'download_uri' => false,
            ])
            ->add('videoFile', FileType::class, [
                'label' => 'video',
                'required' => false,
            ])
            ->add('typeArticle', ChoiceType::class, [
                'label' => 'Type d\'article',
                'choices' => [
                    'image' => 'image',
                    'video' => 'video',
                ],
            ])
            ->add('qui', ChoiceType::class, [
                'label' => 'De qui qu\'on cause ?',
                'choices' => $this->listeSelect(),
            ])
            ->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ModifieArticleCommand::class,
        ]);
    }

    /**
     * @return array<string, int>
     */
    private function listeSelect(): array
    {
        $tabKids = [];
        foreach (Article::KIDS as $kid) {
            $tabKids[$kid['prenom']] = $kid['id'];
        }

        return $tabKids;
    }

    /**
     * @param ModifieArticleCommand|null $viewData
     */
    public function mapDataToForms($viewData, $forms): void
    {
        if (null === $viewData) {
            return;
        }

        if (!$viewData instanceof ModifieArticleCommand) {
            throw new UnexpectedTypeException($viewData, ModifieArticleCommand::class);
        }

        /** @var FormInterface[] $forms */
        $forms = iterator_to_array($forms);
        $forms['id']->setData($viewData->getId());
        $forms['dateArticle']->setData($viewData->getDate());
        $forms['titre']->setData($viewData->getTitre());
        $forms['description']->setData($viewData->getDescription());
        $forms['imageFile']->setData($viewData->getImageFile());
        $forms['videoFile']->setData($viewData->getVideoFile());
        $forms['typeArticle']->setData($viewData->getType());
        $forms['qui']->setData($viewData->getQui());
    }

    public function mapFormsToData($forms, &$viewData): void
    {
        $forms = iterator_to_array($forms);
        /** @var \DateTimeImmutable $dateArticle */
        $dateArticle = $forms['dateArticle']->getData();
        /** @var string $titre */
        $titre = $forms['titre']->getData();
        /** @var string $description */
        $description = $forms['description']->getData();
        /** @var File $imageFile */
        $imageFile = $forms['imageFile']->getData();
        /** @var UploadedFile $videoFile */
        $videoFile = $forms['videoFile']->getData();
        /** @var string $typeArticle */
        $typeArticle = $forms['typeArticle']->getData();
        /** @var int $qui */
        $qui = $forms['qui']->getData();
        $visuel = 'visuelString';
        $video = 'videoString';
        /** @var int $id */
        $id = $forms['id']->getData();

        $viewData = ModifieArticleCommand::fabrique(
            id: $id,
            titre: $titre,
            description: $description,
            date: $dateArticle,
            imageFile: $imageFile,
            videoFile: $videoFile,
            type: $typeArticle,
            qui: $qui,
            visuel: $visuel,
            video: $video
        );
    }
}
