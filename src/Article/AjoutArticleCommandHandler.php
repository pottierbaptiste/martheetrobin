<?php

namespace App\Article;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;

class AjoutArticleCommandHandler
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function execute(AjoutArticleCommand $ajoutArticleCommand): Article
    {
        $article = new Article(
            titre: $ajoutArticleCommand->titre,
            dateArticle: $ajoutArticleCommand->date,
            typeArticle: $ajoutArticleCommand->type,
        );
        $article
            ->setDescription($ajoutArticleCommand->description)
            ->setImageFile($ajoutArticleCommand->imageFile)
            ->setVideoFile($ajoutArticleCommand->videoFile)
            ->setQui($ajoutArticleCommand->qui);
        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }
}
