<?php

namespace App\Article;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;

class ModifieArticleCommandHandler
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ArticleRepository $articleRepository)
    {
    }

    public function execute(ModifieArticleCommand $modifieArticleCommand): Article
    {
        /** @var Article $article */
        $article = $this->articleRepository->find($modifieArticleCommand->id);
        $article
            ->setDateArticle($modifieArticleCommand->date)
            ->setTitre($modifieArticleCommand->titre)
            ->setDescription($modifieArticleCommand->description)
            ->setImageFile($modifieArticleCommand->imageFile)
            ->setVideoFile($modifieArticleCommand->videoFile)
            ->setTypeArticle($modifieArticleCommand->type)
            ->setQui($modifieArticleCommand->qui);
        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }
}
