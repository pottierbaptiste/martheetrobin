<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @extends  ServiceEntityRepository<Article>
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Paginator<mixed>
     */
    public function findAllPagineEtTrie(int $page, int $nbMaxParPage): Paginator
    {
        if (!is_numeric($page)) {
            throw new \InvalidArgumentException('La valeur de l\'argument $page est incorrecte (valeur : '.$page.').');
        }

        if ($page < 1) {
            throw new NotFoundHttpException('La page demandée n\'existe pas');
        }

        if (!is_numeric($nbMaxParPage)) {
            throw new \InvalidArgumentException('La valeur de l\'argument $nbMaxParPage est incorrecte (valeur : '.$nbMaxParPage.').');
        }

        $query = $this->createQueryBuilder('article')
            ->leftJoin('article.avis', 'avis')
            ->leftJoin('article.likes', 'likes')
            ->addSelect('avis')
            ->addSelect('likes')
            ->addOrderBy('article.dateArticle', 'DESC')
            ->addOrderBy('article.id', 'DESC')
            ->getQuery();

        $premierResultat = ($page - 1) * $nbMaxParPage;
        $query->setFirstResult($premierResultat)->setMaxResults($nbMaxParPage)->enableResultCache(30, 'paginatedPage'.$page);
        $paginator = new Paginator($query);

        if (($paginator->count() <= $premierResultat) && 1 != $page) {
            throw new NotFoundHttpException('La page demandée n\'existe pas.'); // page 404, sauf pour la première page
        }

        return $paginator;
    }

    /**
     * @return Paginator<mixed>
     */
    public function searchTerms(string $elementRecherche, int $page, int $nbMaxParPage): Paginator
    {
        $query = $this->createQueryBuilder('a')
            ->Where('a.titre LIKE :what')
            ->orWhere('a.description LIKE :what')
            ->orderBy('a.dateArticle', 'DESC')
            ->setParameter('what', '%'.$elementRecherche.'%')
            ->getQuery();

        $premierResultat = ($page - 1) * $nbMaxParPage;
        $query->setFirstResult($premierResultat)->setMaxResults($nbMaxParPage);
        $paginator = new Paginator($query);

        if (($paginator->count() <= $premierResultat) && 1 != $page) {
            throw new NotFoundHttpException('La page demandée n\'existe pas.'); // page 404, sauf pour la première page
        }

        return $paginator;
    }

    public function getFromSlug(string $slug): mixed
    {
        return $this->createQueryBuilder('a')
            ->Where('a.slug LIKE :slug')
            ->leftJoin('a.avis', 'avis')
            ->addSelect('avis')
            ->leftJoin('a.likes', 'likes')
            ->addSelect('likes')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
