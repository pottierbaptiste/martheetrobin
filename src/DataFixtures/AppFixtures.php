<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $encoder)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $devPassword = 'pwdDev';

        $userBaptiste = new User();
        $userBaptiste->setPassword($this->encoder->hashPassword($userBaptiste, $devPassword))
            ->setEmail('pottierbaptiste@gmail.com')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setUsername('Papa');
        $manager->persist($userBaptiste);

        for ($i = 1; $i <= 99; ++$i) {
            $article = new Article(
                titre: 'Titre '.$i,
                dateArticle: new \DateTimeImmutable(),
                typeArticle: 'image',
            );
            $article
                ->setDescription('Voici une description '.$i)
                ->setQui((int) random_int(0, 2))
                ->setVisuel('photo-'.$i.'.jpeg');
            $manager->persist($article);
        }

        $manager->flush();
    }
}
